const texts = require('../texts');

async function sendHelpMessage(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.help(msg));
}

module.exports = { sendHelpMessage };

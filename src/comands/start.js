const keyboards = require('../keyboards');
const texts = require('../texts');

async function sendStartMessage(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.start(msg), {
    reply_markup : {
      keyboard : keyboards.homeButtons
    }
  });
}

async function sendAdminStartMessage(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.start(), {
    reply_markup : {
      keyboard : keyboards.adminStart
    }
  })
}

module.exports = { sendStartMessage, sendAdminStartMessage };

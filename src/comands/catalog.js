const Cards = require('../models/product.model');
const texts = require('../texts');
const actions = require('../actions');
const Keyboard = require('../models/keyboard.model');
const { productLimit } = require('../config');

const getLastPage = async () => {
  const length = await Cards.find().estimatedDocumentCount();
  return Math.ceil(length / productLimit);
};

const getProductsForTheCurrentPage = async (currentPage) => {
  try {
    return await Cards.find().skip(currentPage * productLimit).limit(productLimit);
  } catch (e) {
    throw new Error(texts.system.baseError());
  }
};

async function timeOut(ms) {
  await new Promise((resolve) => setTimeout(resolve, ms));
}

async function sendCurrentPageProducts(bot, chatId, currentPage = 0) {
  const lastPage = await getLastPage();
  if (currentPage < lastPage) {
    const advertisements = await getProductsForTheCurrentPage(currentPage);
    for (let i = 0; i < advertisements.length; i++) {
      await bot.sendPhoto(chatId, advertisements[i].picture, await getCardsKeyboard(advertisements[i], i === advertisements.length - 1, currentPage));
      await timeOut(100);
    }
  } else {
    await bot.sendMessage(chatId, texts.catalog.endOfTheList());
  }
}

async function createButton(text, actionType, advId, currentPage) {

  if (actionType === actions.LOAD_MORE) {
    return await Keyboard.create({
        text : text,
        callback_data : {
          type : actionType,
          advId : advId,
        },
        count : ++currentPage
      }
    );
  } else {
    return await Keyboard.create({
        text : text,
        callback_data : {
          type : actionType,
          advId : advId,
        }
      }
    );
  }
}

async function getCardsKeyboard(advertisement, lastItem, currentPage) {

  const buy = await createButton(texts.inlineKeyboard.buy(), actions.ADD_TO_SHOPPING_CART, advertisement._id);

  const shoppingCart = await createButton(texts.inlineKeyboard.shoppingCart(), actions.SHOW_SHOPPING_CART, advertisement._id);

  const loadMore = await createButton(texts.inlineKeyboard.loadMore(), actions.LOAD_MORE, advertisement._id, currentPage);

  let inlineKeyboard = [ [ {
    text : buy.text,
    callback_data : buy._id
  },
    {
      text : shoppingCart.text,
      callback_data : shoppingCart._id
    } ] ];

  let lastInlineKeyboard = inlineKeyboard.concat([ [ {
    text : loadMore.text,
    callback_data : loadMore._id
  } ] ]);
  return {
    caption : advertisement.name,
    reply_markup : {
      inline_keyboard : lastItem ? lastInlineKeyboard : inlineKeyboard
    }
  };
}

module.exports = { sendCurrentPageProducts };

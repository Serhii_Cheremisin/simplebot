const Cards = require('../models/product.model');
const ShoppingCart = require('../models/shoppingCart.model');
const keyboards = require('../keyboards');
const texts = require('../texts');

async function addToShoppingCart(bot, chatId, advId, userid) {

  const thisProduct = await Cards.findOne({ _id : advId });
  const usersShoppingCart = await ShoppingCart.findOne({ userId : userid });

  if (usersShoppingCart === null) {
    await ShoppingCart.create({ userId : userid, products : [ { productId : advId, quantity : 1 } ] });
  } else {
    await updateShoppingCart();
  }

  async function updateShoppingCart() {
    let productIndex = usersShoppingCart.products.findIndex(item => item.productId.toString() === advId.toString());
    if (productIndex === -1) {
      usersShoppingCart.products.push({ productId : thisProduct._id, quantity : 1 });
      await ShoppingCart.updateOne({ userId : userid }, { $set : { products : usersShoppingCart.products } });
    } else {
      usersShoppingCart.products[productIndex].quantity++;
      await ShoppingCart.updateOne({ userId : userid }, { products : usersShoppingCart.products });
    }
  }

  await bot.sendMessage(chatId, texts.shoppingCart.add());
}

async function sendShoppingCart(bot, chatId, userid) {

  const item = await ShoppingCart.findOne({ userId : userid });

  if (item === null) {
    return await bot.sendMessage(chatId, texts.shoppingCart.empty());
  } else {
    let allTexts = '';
    let allProducts = 0;
    let total = 0;
    for (let i = 0; i < item.products.length; i++) {
      const product = await Cards.findOne({ _id : item.products[i].productId });
      allTexts += texts.shoppingCart.productText(product.name, product.price, item.products[i].quantity);
      total += +product.price * +item.products[i].quantity;
      allProducts += +item.products[i].quantity;
    }
    await bot.sendMessage(chatId, await texts.shoppingCart.shoppingCartText(allTexts, allProducts, total), keyboards.shoppingCartButton);
  }
}

module.exports = {
  addToShoppingCart,
  sendShoppingCart,
};

const mongoose = require('mongoose')

const shoppingCartModel = new mongoose.Schema({
  userId : {
    type : Number,
    required : true
  },
  products : {
    type : [Object],
    default : []
  }
})

const ShoppingCart = mongoose.model('shoppingCart', shoppingCartModel);

module.exports = ShoppingCart;

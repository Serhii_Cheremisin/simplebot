const mongoose = require('mongoose');

const statusModel = new mongoose.Schema({
  chatId : {
    type : String,
    required : true
  },
  plugin : {
    type : String,
    required : true
  },
  action : {
    type : String,
    required : true
  }
});

const Status = mongoose.model('status', statusModel);

module.exports = Status;
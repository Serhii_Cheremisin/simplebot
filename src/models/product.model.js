const mongoose = require('mongoose');

const productModel = new mongoose.Schema({
  name : {
    type : String,
    required : true
  },
  picture : {
    type : String,
    required : true
  },
  price : {
    type : String,
    required : true
  }
});
const Cards = mongoose.model('cards', productModel);

module.exports = Cards;

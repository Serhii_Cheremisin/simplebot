const mongoose = require('mongoose');

const keyboardSchema = new mongoose.Schema({
  text : {
    type : String,
    required : true
  }
  ,
  callback_data : {
    type : Object,
    required : true
  },
  count : {
    type : Number,
    required : false
  }
});
const Keyboard = mongoose.model('keyboard', keyboardSchema);

module.exports = Keyboard;

const ACTION_TYPE = {
  ADD_TO_SHOPPING_CART : 'Add to shopping cart',
  SHOW_SHOPPING_CART : 'Show shopping cart',
  LOAD_MORE : 'Load more',
  ADD_TITLE : 'Add title',
  ADD_PICTURE : 'Add picture',
  ADD_PRICE : 'Add price',
  FIND_PRODUCT : 'Find product',
  UPDATE_TITLE : 'Update title',
  UPDATE_PICTURE : 'Update picture',
  UPDATE_PRICE : 'Update price',
  DELETE_PRODUCT : 'Delete product'
};

module.exports = ACTION_TYPE;

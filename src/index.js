const TelegramBot = require('node-telegram-bot-api');
const mongoose = require('mongoose');
const { MONGODB_URI, ADMIN } = require('./config');
const actions = require('./actions');
const texts = require('./texts');
const { addToShoppingCart } = require('./comands/shoppingCart');
const { sendCurrentPageProducts } = require('./comands/catalog');
const { sendStartMessage, sendAdminStartMessage } = require('./comands/start');
const { sendHelpMessage } = require('./comands/help');
const { sendShoppingCart } = require('./comands/shoppingCart');
const { addProduct, addTitle, addPicture, addPrice } = require('./storeManagement/addProduct');
const {
  updateProduct,
  updateTitle,
  updatePicture,
  updatePrice,
  findProduct
} = require('./storeManagement/updateProduct');
const { deleteProduct, deleteThisProduct } = require('./storeManagement/deleteProduct');
const { statusHandler } = require('./storeManagement/statusHandler');
const ShoppingCart = require('./models/shoppingCart.model');
const Keyboard = require('./models/keyboard.model');
const Status = require('./models/status');

mongoose.connect(MONGODB_URI, {
  useNewUrlParser : true,
  useUnifiedTopology : true
}).then(() => console.log(texts.system.connected()))
  .catch(e => console.error(e));

const bot = new TelegramBot(process.env.TOKEN, { polling : true });

ShoppingCart.deleteMany().catch(e => console.error(e));

Keyboard.deleteMany().catch(e => console.error(e));

Status.deleteMany().catch(e => console.error(e));

bot.onText(/\/start/, async msg => {
  if (msg.chat.id === ADMIN) {
    await sendAdminStartMessage(bot, msg);
  } else {
    await sendStartMessage(bot, msg);
  }
});

bot.onText(/\/help/, async msg => {
  await sendHelpMessage(bot, msg);
});

bot.onText(/Каталог/, async msg => {
  await sendCurrentPageProducts(bot, msg.chat.id);
});

bot.onText(/Корзина/, async msg => {
  await sendShoppingCart(bot, msg.chat.id, msg.from.id,);
});




bot.onText(/Добавить товар/, async msg => {
  await addProduct(bot, msg);
});

bot.onText(/Изменить товар/, async msg => {
  await updateProduct(bot, msg);
});

bot.onText(/Добавить название/, async msg => {
  await addTitle(bot, msg);
});

bot.onText(/Изменить название/, async msg => {
  await updateTitle(bot, msg);
});

bot.onText(/Добавить изображение/, async msg => {
  await addPicture(bot, msg);
});

bot.onText(/Изменить изображение/, async msg => {
  await updatePicture(bot, msg);
});

bot.onText(/Добавить цену/, async msg => {
  await addPrice(bot, msg);
});

bot.onText(/Изменить цену/, async msg => {
  await updatePrice(bot, msg);
});

bot.onText(/Найти товар/, async msg => {
  await findProduct(bot, msg);
});

bot.onText(/Удалить товар/, async msg => {
  await deleteProduct(bot, msg);
});

bot.onText(/Удалить данный товар/, async msg => {
  await deleteThisProduct(bot, msg);
});

bot.on('callback_query', async query => {
  const id = query.from.id;
  const data = await Keyboard.findById(query.data);

  switch (data.callback_data.type) {
    case actions.ADD_TO_SHOPPING_CART:
      await addToShoppingCart(bot, query.message.chat.id, data.callback_data.advId, id);
      break;
    case actions.SHOW_SHOPPING_CART:
      await sendShoppingCart(bot, query.message.chat.id, id);
      break;
    case actions.LOAD_MORE:
      await sendCurrentPageProducts(bot, query.message.chat.id, data.count);
      break;
  }
});

bot.on('message', async msg => {
  await statusHandler(bot, msg);
});

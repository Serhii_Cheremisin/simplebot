const Status = require('../models/status');
const Cards = require('../models/product.model');
const { sendAdminStartMessage } = require('../comands/start');

let newProduct = {};

async function findProductByName(bot, msg) {
  const product = await Cards.findOne({ name : msg.text });
  if (product !== null) {
    await bot.sendMessage(msg.chat.id, 'Внесите необходимые изменения');
    newProduct = product;
  } else {
    await bot.sendMessage(msg.chat.id, 'Товар с таким названием не найден');
  }
  await Status.deleteOne();
}


async function setTitle(msg) {
  newProduct.name = msg.text;
  await Status.deleteOne();
}

async function setPicture(msg) {
  newProduct.picture = msg.text;
  await Status.deleteOne();
}

async function setPrice(bot, msg) {
  newProduct.price = msg.text;
  if ( !newProduct._id) {
    await addProductToDB(bot, msg);
  } else {
    await updateProductInDB(bot, msg);
  }
  await Status.deleteOne();
}

async function addProductToDB(bot, msg) {
  await Cards.create(newProduct);
  await bot.sendMessage(msg.chat.id, 'Товар успешно добавлен');
  await sendAdminStartMessage(bot, msg);
}

async function updateProductInDB(bot, msg) {
  await Cards.updateOne({ _id : newProduct._id }, {
    $set : {
      name : newProduct.name,
      picture : newProduct.picture,
      price : newProduct.price
    }
  });
  await bot.sendMessage(msg.chat.id, 'Товар успешно изменён');
  await sendAdminStartMessage(bot, msg);
}

async function deleteProductInDB(bot, msg) {
  await Cards.deleteOne({ _id : newProduct._id });
  await bot.sendMessage(msg.chat.id, 'Товар успешно удалён');
  await sendAdminStartMessage(bot, msg);
}

module.exports = {
  setTitle,
  setPicture,
  setPrice,
  findProductByName,
  deleteProductInDB
};

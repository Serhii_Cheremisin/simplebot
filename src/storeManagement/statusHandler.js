const { getStatus } = require('./status');
const { setTitle, setPicture, setPrice, findProductByName, deleteProductInDB } = require('../storeManagement/setProduct');
const actions = require('../actions');

async function statusHandler(bot, msg) {
  const currentStatus = await getStatus(msg.chat.id);
  if (currentStatus !== null) {

    switch (currentStatus.action) {
      case actions.ADD_TITLE :
        await setTitle(msg);
        break;
      case actions.ADD_PICTURE :
        await setPicture(msg);
        break;
      case actions.ADD_PRICE :
        await setPrice(bot, msg);
        break;
      case actions.FIND_PRODUCT :
        await findProductByName(bot, msg);
        break;
      case actions.UPDATE_TITLE :
        await setTitle(msg);
        break;
      case actions.UPDATE_PICTURE :
        await setPicture(msg);
        break;
      case actions.UPDATE_PRICE :
        await setPrice(bot, msg);
        break;
      case actions.DELETE_PRODUCT :
        await deleteProductInDB(bot, msg);
        break;
    }
  }
}

module.exports = {
  statusHandler
};
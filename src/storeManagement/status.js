const Status = require('../models/status');

async function createStatus(msg, action, plugin) {
  await Status.create({
    chatId : msg.chat.id,
    plugin : plugin,
    action : action
  });
}

async function getStatus(id) {
  return Status.findOne({ chatId : id });
}

module.exports = {
  createStatus,
  getStatus
};


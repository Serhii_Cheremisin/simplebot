const { createStatus } = require('../storeManagement/status');
const keyboards = require('../keyboards');
const actions = require('../actions');

async function deleteProduct(bot, msg) {
  await bot.sendMessage(msg.chat.id, 'Удаление товара', {
    reply_markup : {
      keyboard : keyboards.deleteProduct
    }
  });
}

async function deleteThisProduct(bot, msg) {
  await bot.sendMessage(msg.chat.id, 'Отправьте "да", для удаления');
  await createStatus(msg, actions.DELETE_PRODUCT, 'storeManagement');
}

module.exports = {
  deleteProduct,
  deleteThisProduct
};
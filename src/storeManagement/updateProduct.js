const { createStatus } = require('../storeManagement/status');
const texts = require('../texts');
const keyboards = require('../keyboards');
const actions = require('../actions');

async function updateProduct(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.updateProduct(), {
    reply_markup : {
      keyboard : keyboards.updateProduct
    }
  });
}

async function findProduct(bot, msg) {
  await bot.sendMessage(msg.chat.id, 'Введите название товара');
  await createStatus(msg, actions.FIND_PRODUCT,'storeManagement');
}

async function updateTitle(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.waitingName());
  await createStatus(msg, actions.UPDATE_TITLE, 'storeManagement');
}

async function updatePicture(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.waitingPicture());
  await createStatus(msg, actions.UPDATE_PICTURE, 'storeManagement');
}

async function updatePrice(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.waitingPrice());
  await createStatus(msg, actions.UPDATE_PRICE, 'storeManagement');
}

module.exports = {
  findProduct,
  updateProduct,
  updateTitle,
  updatePicture,
  updatePrice
};

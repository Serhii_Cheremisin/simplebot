const { createStatus } = require('../storeManagement/status');
const texts = require('../texts');
const keyboards = require('../keyboards');
const actions = require('../actions');

async function addProduct(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.addProduct(), {
    reply_markup : {
      keyboard : keyboards.addProduct
    }
  });
}

async function addTitle(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.waitingName());
  await createStatus(msg, actions.ADD_TITLE, 'storeManagement');
}

async function addPicture(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.waitingPicture());
  await createStatus(msg, actions.ADD_PICTURE, 'storeManagement');
}

async function addPrice(bot, msg) {
  await bot.sendMessage(msg.chat.id, texts.admin.waitingPrice());
  await createStatus(msg, actions.ADD_PRICE, 'storeManagement');
}

module.exports = {
  addProduct,
  addTitle,
  addPicture,
  addPrice
};

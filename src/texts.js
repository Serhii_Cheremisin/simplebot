module.exports = {
  start : (msg) => {
    return `Здравствуйте, ${ msg.from.first_name }!\n Рады видеть Вас в нашем магазине!`;
  },
  help : (msg) => {
    return `Здравствуйте, ${ msg.from.first_name }!\n Для работы с данным ботом используйте команду /start`;
  },
  shoppingCart : {
    productText : (name, price, quantity) => {
      return `Название: ${ name } \n Цена: ${ price } грн \n x ${ quantity } \n \n`;
    },
    shoppingCartText : (allTexts, allProducts, total) => {
      return `Корзина:\n \n ${ allTexts } Всего товаров: ${ allProducts } шт.\n Общая сумма заказа:  ${ total } грн`;
    },
    add : () => {
      return 'Данный товар добавлен в корзину';
    },
    empty : () => {
      return 'Корзина пуста. Сначала добавьте товар';
    }
  },
  catalog : {
    endOfTheList : () => {
      return 'Это все товары, для просмотра каталога с начала нижмите "Каталог"';
    }
  },
  inlineKeyboard : {
    buy : () => {
      return 'Купить';
    },
    shoppingCart : () => {
      return 'Корзина';
    },
    loadMore : () => {
      return 'Показать ещё';
    }
  },
  system : {
    connected : () => {
      return 'Connected';
    },
    baseError : () => {
      return 'Did not receive products for the current page';
    }
  },
  admin : {
    start : () => {
      return 'Добрый день, администратор. Выберите действие...';
    },
    addProduct : () => {
      return 'Создание нового товара...';
    },
    updateProduct : () => {
      return 'Изменение товара...'
    },
    waitingName : () => {
      return 'Введите название';
    },
    waitingPicture : () => {
      return 'Введите URL изображения';
    },
    waitingPrice : () => {
      return 'Введите стоимость';
    }
  }
};
